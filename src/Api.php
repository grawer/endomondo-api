<?php

namespace Grawer\EndomondoApi;

use Ramsey\Uuid\Uuid;

class Api
{
    const URL_AUTH = 'auth';
    const URL_BASE = 'https://api.mobile.endomondo.com/mobile/';
    const URL_CHALLENGE = 'api/challenge/get';

    protected $curl;

    public function __construct($email, $password, $authToken = null)
    {
        $this->email = $email;
        $this->password = $password;
        $this->authToken = $authToken;
    }

    public function __destruct()
    {
        if ($this->curl !== null) {
            curl_close($this->curl);
        }
    }

    public function getAuthToken()
    {
        if (!isset($this->authToken[0])) {
            $this->authToken = $this->requestAuthToken($this->email, $this->password);
        }

        return $this->authToken;
    }

    public function requestAuthToken($email, $password)
    {
        $parameters = array(
            'email'         => $email,
            'password'      => $password,
            'country'       => 'PL',
            'deviceId'      => (string)Uuid::uuid5(Uuid::NAMESPACE_DNS, gethostname()),
            'os'            => 'Android',
            'appVersion'    => '7.1',
            'appVariant'    => 'M-Pro',
            'osVersion'     => '2.3.6',
            'model'         => 'GT-B5512',
            'v'             => 2.4,
            'action'        => 'PAIR'
        );

        $data = $this->doRequest(self::URL_BASE . self::URL_AUTH, $parameters);

        if (substr($data, 0, 2) == 'OK') {
            $lines = explode("\n", $data);
            $authLine = explode('=', $lines[2]);

            return $authLine[1];
        }

        return false;
    }

    public function doRequest($url, $fields = null)
    {
        $queryString = http_build_query($fields);
        $url = $url . '?' . $queryString;

        if ($this->curl == null) {
            $this->curl = curl_init();
        }

        if (strpos($url, 'https://') === 0) {
            curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
        }

        curl_setopt_array($this->curl, array(
            CURLOPT_HEADER          => false,
            CURLOPT_REFERER         => $url,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_URL             => $url,
            CURLOPT_USERAGENT       => 'Dalvik/1.4.0 (Linux; U; Android 2.3.6; GT-B5512 Build/GINGERBREAD)',
        ));

        $data = curl_exec($this->curl);

        return $data;
    }

    public function getChallenge($challengeId)
    {
        $parameters = array(
            'authToken'     => $this->getAuthToken(),
            'challengeId'   => $challengeId,
            'fields'        => 'basic,leaderboard,total,size',
        );

        $challenge = $this->doRequest(self::URL_BASE . self::URL_CHALLENGE, $parameters);
        
        return json_decode($challenge, true);
    }
}
