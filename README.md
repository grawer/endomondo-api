# Endomondo API #

A minimal implementation of Endomondo Api for getting challenge data.

### Basic usage ###


```
#!php

$email = 'your-endomondo-email';
$password = 'your-endomondo-password';

$api = new Grawer\EndomondoApi\Api($email, $password);
$challenge = $api->getChallenge($challengeId);

```

### Advanced usage ###

You can (and should) save authentication token for later use, and don't do request for authentication if not needed.

```
#!php

$email = 'your-endomondo-email';
$password = 'your-endomondo-password';

$api = new Grawer\EndomondoApi\Api($email, $password);
$authToken = $api->getAuthToken();
file_put_contents('auth_token.txt', $authToken);

```

and then next time:

```
#!php

$email = 'your-endomondo-email';
$password = 'your-endomondo-password';
$authToken = file_get_contents('auth_token.txt');

$api = new Grawer\EndomondoApi\Api($email, $password, $authToken);

```
